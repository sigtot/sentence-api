package main

import (
	"net/http"
	"strings"
	"strconv"
	"fmt"
	"math/rand"
	"github.com/coreos/bbolt"
	"errors"
	"time"
	"encoding/json"
)

const apiVersion = "V0.1"
const apiRoot = "/api/" + apiVersion + "/"

const minLength = 2
const maxLength = 13

const defaultNumSentences = 10

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

// Returns a random number in the range [min,max] (inclusive)
func randomInRange(min, max int) int {
	return rand.Intn(max - min + 1) + min
}

// The coolest distribution you've ever seen.
// bounceLeft = bounce on the left edge
// bounceRight = bounce on the right edge
func coolDistribution(expected, minVal, maxVal int, bounceLeft, bounceRight bool) int {
	var depth int
	l := expected - minVal
	r := maxVal - expected

	if l < r {
		if bounceLeft {
			depth = r
		} else {
			depth = l
		}
	}

	if l > r {
		if bounceRight {
			depth = l
		} else {
			depth = r
		}
	}

	res := expected
	for i := 0; i < depth; i++ {
		d := randomInRange(-1, 1)
		if res == minVal && d == -1 && bounceLeft {
			d = 1
		}

		if res == maxVal && d == 1 && bounceRight {
			d = -1
		}

		res += d
	}
	return res
}

// Write sentences to responseWriter.
// Data takes the form ["sentence",num,length], ["sentence",num] or ["sentence"]
func writeSentences(w http.ResponseWriter, data []string) error {
	if len(data) > 3 {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return nil
	}

	var num int
	var randomLength bool = true
	var length int
	if len(data) > 1 {
		var err error
		num, err = strconv.Atoi(data[1])
		if err != nil {
			return err
		}

		if len(data) > 2 {
			randomLength = false
			var err error
			length, err = strconv.Atoi(data[2])
			if err != nil {
				return err
			}

			if length < minLength || length > maxLength {
				http.Error(w, "400 bad request: invalid length requested. min: 2 words, max: 13 words", http.StatusBadRequest)
				return nil
			}
		}
	} else {
		num = defaultNumSentences
	}


	var sentences []string
	if randomLength {
		// TODO: Decide length with coolDistribution and send

	} else {
		var numAvailSentences int
		err := db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(strconv.Itoa(length)))
			if b == nil {
				http.Error(w, "500 internal server error", http.StatusInternalServerError)
				fmt.Printf("Error: Requested sentence length %d does not correspond to a bucket\n", length)
				return errors.New("No bucket matching length")
			}

			var err error
			numAvailSentences, err = strconv.Atoi(string(b.Get([]byte("last_id"))))
			if err != nil {
				return err
			}

			return nil
		})
		if err != nil {
			return err
		}

		if num > numAvailSentences {
			http.Error(w, "400 bad request: too many sentences requested", http.StatusBadRequest)
			return nil
		}

		availSentenceIds := rand.Perm(numAvailSentences)

		sentenceIds := availSentenceIds[:num]
		db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(string(strconv.Itoa(length))))
			for i := 0; i < len(sentenceIds); i++ {
				sentences = append(sentences, string(b.Get([]byte(strconv.Itoa(sentenceIds[i] + 1)))))
			}
			return nil
		})
	}

	j, err := json.Marshal(sentences)
	if err != nil {
		fmt.Printf("Could not marshal sentence array: %v\n", err)
		return err
	}

	w.Write(j)

	return nil
}

func apiHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}

	data := strings.Split(strings.TrimPrefix(r.URL.Path, apiRoot), "/")

	if len(data) == 0 {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}

	switch data[0] {
	case "sentence":
		err := writeSentences(w, data)
		if err != nil {
			fmt.Printf("Could not write sentences: %s\n", err)
			http.Error(w, "500 internal server error", http.StatusInternalServerError)
		}
	default:
		http.Error(w, "404 page not found", http.StatusNotFound)
	}
}

var db *bolt.DB

func main() {
	var err error
	db, err = bolt.Open("sentences.db", 0600, &bolt.Options{Timeout: time.Second})
	if err != nil {
		fmt.Errorf("Could not open database %s\n", err)
	}
	http.HandleFunc(apiRoot, apiHandler)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		panic(err)
	}
}
