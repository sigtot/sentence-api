# Sentence API
Version 0.1

### Requirements:
A [bolt](https://github.com/coreos/bbolt) database of sentences needs to be generated with
[gitlab.com/sigtot/sentence-finder](https://gitlab.com/sigtot/sentence-finder) and placed in 
the root directory.

### Usage:
The API runs on port `5000` by default.
Send a `GET` request to `/api/V0.1/sentence/{number}/{length}` to retrieve `{number}` sentences of
length `{length}`.\
Note: Length is the number of words in the sentence.

The length and number parameters are optional so requesting
* `/api/V0.1/sentence` would return __10__ (default) sentences of random length
* `/api/V0.1/sentence/5` would return __5__ sentences of random length
